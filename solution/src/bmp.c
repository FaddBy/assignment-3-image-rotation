#include "include/bmp.h"
#include "include/image.h"

#include <inttypes.h>
#include <malloc.h>

static inline uint8_t calc_padding(uint32_t width)
{
    return (4 - ((width * sizeof(struct pixel)) % 4)) % 4;
}

enum read_status from_bmp(FILE *input, struct image *img, struct bmp_header *header)
{
    if (img == NULL)
    {
        printf("from_bmp - img_null\n");
        return READ_INVALID_HEADER;
    }
    if (fread(header, sizeof(struct bmp_header), 1, input) < 1)
    {
        printf("from_bmp - fread_error\n");
        return READ_INVALID_HEADER;
    }
    if (header->bfType != 0x4D42 || header->biBitCount != 24 || header->biCompression != 0)
    {
        printf("from_bmp - invalid_header\n");
        return READ_INVALID_HEADER;
    }
    img->data = (struct pixel *)malloc(sizeof(struct pixel) * header->biWidth * header->biHeight);
    img->width = header->biWidth;
    img->height = header->biHeight;
    uint8_t padding = calc_padding(img->width);
    for (uint32_t i = 0; i < img->height; ++i)
    {
        for (uint32_t j = 0; j < img->width; ++j)
        {
            fread(&img->data[img->width * (img->height - i - 1) + j], sizeof(struct pixel), 1, input);
        }
        fseek(input, padding, SEEK_CUR);
    }
    return READ_OK;
}

enum write_status to_bmp(FILE *output, const struct image *img, struct bmp_header *old_header)
{
    if (img == NULL)
    {
        printf("to_bmp - image is null\n");
        return WRITE_ERROR;
    }
    uint8_t padding = calc_padding(img->width);
    uint32_t size = (img->width * sizeof(struct pixel) + padding) * img->height;
    struct bmp_header new_header = *old_header;
    new_header.biWidth = img->width;
    new_header.biHeight = img->height;
    new_header.biSizeImage = size;
    fwrite(&new_header, sizeof(struct bmp_header), 1, output);
    uint32_t zero = 0;

    for (uint32_t i = 0; i < img->height; ++i)
    {
        for (uint32_t j = 0; j < img->width; ++j)
        {
            fwrite(&img->data[img->width * (img->height - i - 1) + j], sizeof(struct pixel), 1, output);
        }
        fwrite(&zero, 1, padding, output);
    }
    if (ferror(output))
    {
        return WRITE_ERROR;
    }

    return WRITE_OK;
}

void bmp_header_print(const struct bmp_header *header)
{
    printf("bfType: %hu\n", header->bfType);
    printf("bfileSize: %u\n", header->bfileSize);
    printf("bfReserved: %u\n", header->bfReserved);
    printf("bfOffBits: %u\n", header->bfOffBits);
    printf("biSize: %u\n", header->biSize);
    printf("biWidth: %u\n", header->biWidth);
    printf("biHeight: %u\n", header->biHeight);
    printf("biPlanes: %hu\n", header->biPlanes);
    printf("biBitCount: %hu\n", header->biBitCount);
    printf("biCompression: %u\n", header->biCompression);
    printf("biSizeImage: %u\n", header->biSizeImage);
    printf("biXPelsPerMeter: %u\n", header->biXPelsPerMeter);
    printf("biYPelsPerMeter: %u\n", header->biYPelsPerMeter);
    printf("biClrUsed: %u\n", header->biClrUsed);
    printf("biClrImportant: %u\n", header->biClrImportant);
}
