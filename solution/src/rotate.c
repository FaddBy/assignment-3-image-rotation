#include "include/image.h"
#include "include/bmp.h"

#include <inttypes.h>
#include <stdlib.h>


struct image rotate(struct image src)
{
    struct image dst;

    dst.data = (struct pixel *) malloc(sizeof(struct pixel) * (size_t ) src.width * (size_t) src.height);
    dst.width = src.height;
    dst.height = src.width;
    for (uint64_t x = 0; x < dst.width; x++) {
        for (uint64_t y = 0; y < dst.height; y++) {
            uint64_t src_x = dst.height - y - 1;
            uint64_t src_y = x;

            dst.data[x + y * dst.width] = src.data[src_x + src_y * src.width];
        }
    }

    free(src.data);
    

    return dst;
    
}
