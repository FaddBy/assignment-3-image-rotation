#ifndef ROTATE_H
#define ROTATE_H

#include "image.h"

struct image rotate(struct image src); 

#endif /* ROTATE_H */
