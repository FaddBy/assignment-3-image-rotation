
#include "include/image.h"

#include <malloc.h>

void copy_image(const struct image *restrict src, struct image *restrict dst)
{
    if (!src || !dst || !src->data || !dst->data)
        return;
    dst->width = src->width;
    dst->height = src->height;
    for (int i = 0; i < src->width * src->height; i++)
    {
        dst->data[i].r = src->data[i].r;
        dst->data[i].g = src->data[i].g;
        dst->data[i].b = src->data[i].b;
    }
}

void free_image(struct image *image)
{
    if (!image || !image->data)
        return;
    free(image->data);
    image->width = 0;
    image->height = 0;
}

void create_image(struct image *dst, uint64_t width, uint64_t height)
{
    if (!dst)
        return;

    dst->height = height;
    dst->width = width;
    dst->data = (struct pixel *)malloc(dst->width * dst->height * sizeof(struct pixel));

    if (!dst->data)
    {
        dst->height = 0;
        dst->width = 0;
    }
}

void image_print(const struct image *image)
{
    printf("Width: %lu\n", image->width);
    printf("Height: %lu\n", image->height);
    printf("Pixels:\n");
    for (int i = 0; i < image->height; i++)
    {
        for (int j = 0; j < image->width; j++)
        {
            printf("(%d, %d, %d) ", image->data[i * image->width + j].b, image->data[i * image->width + j].g, image->data[i * image->width + j].r);
        }
        printf("\n");
    }
}
