#include "include/image.h"
#include "include/bmp.h"
#include "include/rotate.h"

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{

    switch (argc)
    {
    case 1:
        printf("You need to choose source file and destination file\n");
        return 0;
    case 2:
        printf("You also need to choose destination file\n");
        return 0;
    }
    FILE *src = fopen(argv[1], "r");

    struct image src_image;
    struct bmp_header src_header;
    struct image dst_image;
    struct bmp_header dst_header;
    from_bmp(src, &src_image, &src_header);

    fclose(src);
    dst_header = src_header;
    dst_image = rotate(src_image);
    // dst_image = src_image;
    // for (size_t i = 0; i < 6; i++) {
    //     (dst_image.data + i)->b = 0;
    //     (dst_image.data + i)->r = 0;
    //     (dst_image.data + i)->g = 0;
    // }
    // for (size_t i = 0; i < 1000; i++) {
    //     struct pixel* p = dst_image.data + i;
    //     p->b = 0;
    //     p->g = 0;
    //     p->r = 120;
    // }
    FILE *dst = fopen(argv[2], "w");
    if (dst == NULL)
    {
        printf("main - Failed to open file");
        return 0;
    }
    to_bmp(dst, &dst_image, &dst_header);
    fclose(dst);
    free(dst_image.data);
}
