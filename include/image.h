#ifndef LAB5_IMAGE_H
#define LAB5_IMAGE_H

#include <stdint.h>

struct pixel {
    uint8_t r;
    uint8_t g;
    uint8_t b;
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

void copy_image(const struct image * restrict src, struct image * restrict dst);
void free_image(struct image* image);
void create_image(struct image *dst, uint64_t width, uint64_t height);
void image_print(const struct image* image, char* prefix);

#endif //LAB5_IMAGE_H
