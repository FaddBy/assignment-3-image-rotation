#ifndef LAB5_BMP_H
#define LAB5_BMP_H

#include <stdint.h>
#include <stdio.h>
#include "image.h"

struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bfOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} __attribute__((packed));;

enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER
};

enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR
};

enum read_status from_bmp( FILE * input, struct image * img, struct bmp_header* header);

enum write_status to_bmp( FILE * output, const struct image * img, struct bmp_header* header);

void bmp_header_print(const struct bmp_header* header);

#endif //LAB5_BMP_H
